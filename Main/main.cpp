#include "ModeliFile.hpp"
#include "Simulator.hpp"
#include "ModeliGrpcServer.h"
#include "grpc++/grpc++.h"
#include <chrono>
#include <iostream>
#include <thread>
#include "boost/program_options.hpp"


using grpc::Server;
using grpc::ServerBuilder;

int main(int argc, char *argv[])
{
    namespace po = boost::program_options;
    try
    {
        // parse the command line
        po::options_description desc("Allowed Options");
        desc.add_options()
            ("help,h", "Print help messages")
            ("port,p", po::value<unsigned short>()->default_value(52062), "Port of the TCP server")
            ("modeli,m", po::value<std::string>(), "Path to a modeli file");
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        // --help option
        if (vm.count("help"))
        {
            std::cout << "If no port is specified 52062 is used. "
                << "A modeli file can be loaded via --modeli option on startup. " << desc;
            return 0;
        }
        // actually run the backend
        else
        {
            // Extract the port (default value will always be set)
            unsigned short port = 52062;
            if (vm.count("port"))
            {
                port = vm["port"].as<unsigned short>();
            }
            auto simulator = std::make_unique<Simulation::Simulator>();
            auto file_manager = std::make_shared<Files::FmuFileManager>();
            // Load the modeli file into the engine
            if (vm.count("modeli"))
            {
                try
                {
                    Files::ModeliFile modeliFile(vm["modeli"].as<std::string>());
                    file_manager = modeliFile.get_fmu_file_manager();
                    for (auto instance_and_model : modeliFile.get_model_for_instance())
                    {
                        simulator->AddFmu(instance_and_model.second, instance_and_model.first);
                    }
                    for (auto channelLink : modeliFile.get_channel_links())
                    {
                        simulator->AddChannelLink(channelLink);
                    }
                    // Run the simulation
                    simulator->Play();
                }
                catch (std::exception err)
                {
                    std::cout << "Failed loading the modeli file: " << err.what() << std::endl;
                }
            }
            // Configuration for rpc
            ModeliGrpcServer service(std::move(simulator), file_manager);
            std::string targetUri = "0.0.0.0:" + std::to_string(port);
            // Start the server
            grpc::ServerBuilder builder;
            builder.AddListeningPort(targetUri, grpc::InsecureServerCredentials());
            builder.RegisterService(&service);
            std::unique_ptr<Server> server(builder.BuildAndStart());

            std::cout << "Started listening to: " + targetUri << std::endl;
            std::cout << "Enter exit to close the application." << std::endl;
            // Block until process is killed
            std::string exitString;
            while (exitString != "exit")
            {
                std::cin >> exitString;
            }
            // Shutdown the streams
            service.Shutdown();
            server->Shutdown();
        }
    }
    catch (const po::error &ex)
    {
        std::cerr << ex.what() << std::endl;
    }
    return 0;
}