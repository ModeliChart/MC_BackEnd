#include "QueueDispatcher.h"

namespace Utility
{

    QueueDispatcher::QueueDispatcher()
    {
        // Start in automatic mode
        m_manual = false;
        t_execution = std::thread(std::bind(&QueueDispatcher::execute_loop, this));
    }


    QueueDispatcher::~QueueDispatcher()
    {
        // Stop execution
        set_manual();
    }
    void QueueDispatcher::set_manual()
    {
        m_manual = true;
        if (t_execution.joinable())
        {
            t_execution.join();
        }
    }
    bool QueueDispatcher::execute_one()
    {
        if (m_manual)
        {
            if (auto action = m_queue.pop())
            {
                (*action)();
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    void QueueDispatcher::set_automatic()
    {
        m_manual = false;
        // Execute in new thread
        t_execution = std::thread(std::bind(&QueueDispatcher::execute_loop, this));
    }

    void QueueDispatcher::finish()
    {
        m_queue.finish();
    }

    void QueueDispatcher::execute_loop()
    {
        while (!m_manual)
        {
            if (auto action = m_queue.waitfor_and_pop(100))
            {
                (*action)();
            }
        }
    }
}