#pragma once
#include "ConcurrentQueue.h"
#include <atomic>
#include <future>
#include <functional>
#include <memory>
#include <thread>

namespace Utility
{
    /// Use this class to dispatch actions sequencially.
    /// There are two modes available: Run automaic or execute manually.
    class QueueDispatcher
    {
    public:
        /// Creates a QueueDispacther in automatic mode
        QueueDispatcher();
        /// Execute all pending jobs
        ~QueueDispatcher();

        /// Switch to manual execution
        void set_manual();
        /// Execute one action, returns false if no action is available or automatic mode is active
        bool execute_one();
        /// Switch to automatic execution
        void set_automatic();

        /// The value of the future will be set when the fn has been executed
        template <typename T>
        std::future<T> dispatch_fn(std::function<T()> fn)
        {
            // shared ptr of promise because it is non copyable
            // function needs to be copyable!
            auto promise = std::make_shared<std::promise<T>>();
            auto future = promise->get_future();
            // Wrap the function in an action that sets a promise
            auto wrapperFn = [promise, fn]()
            {
                promise->set_value(fn());
            };
            m_queue.push(wrapperFn);
            return future;
        }

        /// The future will be set when fn has been executed
        std::future<void> dispatch_void(std::function<void()> fn)
        {
            // shared ptr of promise because it is non copyable
            // function needs to be copyable!
            auto promise = std::make_shared<std::promise<void>>();
            auto future = promise->get_future();
            // Wrap the function in an action that sets a promise
            auto wrapperFn = [promise, fn]()
            {
                fn();
                promise->set_value();
            };
            m_queue.push(wrapperFn);
            return future;
        }

        /// The dispatcher won't accept any new jobs after calling this function.
        void finish();

    private:
        /// Queue the actions
        ConcurrentQueue<std::function<void()>> m_queue;
        std::atomic_bool m_manual;
        std::thread t_execution;
        void execute_loop();
    };
}