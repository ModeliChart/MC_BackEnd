# ModeliChart Backend

This projects implements a FMU client which performs the simulation in realtime.
Linux and Windows are supported.

# Requirements

* The protocol has its own project page:
https://git.rwth-aachen.de/ModeliChart/ModeliProtocol.
* Grpc installed
* Boost installed (boost_filesystem;boost_system;boost_program_options;boost_timer)

# Build
Boost and grpc are required for compilation.
The easiest way to obtain the libraries on windows is vcpkg.
The projects are Visual Studio projects one project targets Windows the other one Linux.
Linux installation of the libraries via apt or build from source.

# Style guide
I did not use a style guide for a long time but I am trying to incorporate a 
simple style (like Bjarne Stroustrup suggests) in the future. This style is 
close to the ISO standard libraries :)

http://micro-os-plus.github.io/develop/naming-conventions/#class-names

Documentary comments should be qt style for doxygen.