#pragma once
#include <functional>
#include <string>
#include <vector>
#include "boost/property_tree/ptree.hpp"
#include "boost/filesystem.hpp"

namespace Files
{
    /// Handles the fmu file. On creation the fmu is extracted.
    /// On destruction the extracted files are cleaned up.
    class ModelDescription
    {
    private:
        // FMU info
        std::string m_model_name;
        std::string m_guid;
        // The path where the extracted fmu is stored
        std::string m_model_dir;
        // ValueReferences for each type
        std::vector<unsigned int> m_int_vrs;
        std::vector<unsigned int> m_real_vrs;
        std::vector<unsigned int> m_bool_vrs;
        std::vector<unsigned int> m_string_vrs;

    public:
        /// Initialize from local file
        ModelDescription();
        ~ModelDescription();

        /// Load the modelDescription.xml. Set the model_dir before.
        void load();
        /// Set the directory of the model
        /// The xml is not parsed.
        void set_model_dir(std::string model_dir);
        /// Returns the path to the directory where the fmu has been unzipped
        std::string get_model_dir();
        /// Returns the canonicial path to the binary without extension.
        std::string get_binary_path();
        /// Returns the model identifier
        std::string get_model_name();
        /// Returns the guid from the modelDescription xml
        std::string get_guid();
        /// Create an URI to the resource dir. The uri is "file:///" + absolutePath of modelDir
        std::string get_resource_uri();

        std::vector<unsigned int> get_int_vrs();
        std::vector<unsigned int> get_real_vrs();
        std::vector<unsigned int> get_bool_vrs();
        std::vector<unsigned int> get_string_vrs();
    };
}