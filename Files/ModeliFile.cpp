#include "ModeliFile.hpp"
#include "Unzipper.hpp"
#include "ChannelLink.h"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include "boost/filesystem.hpp"

namespace fs = boost::filesystem;
namespace pt = boost::property_tree;

namespace Files
{
    ModeliFile::ModeliFile(std::string path_to_modeli)
    {
        unzip_modeli_file(path_to_modeli);
        parse_modeli_xml();
    }
    ModeliFile::~ModeliFile()
    {
        fs::remove_all(m_unzip_dir);
    }

    std::vector<Simulation::ChannelLink> ModeliFile::get_channel_links()
    {
        return m_channel_links;
    }

    std::shared_ptr<FmuFileManager> ModeliFile::get_fmu_file_manager()
    {
        return m_file_manager;
    }
    std::map<std::string, ModelDescription> ModeliFile::get_model_for_instance()
    {
        return m_model_for_instance;
    }
    void ModeliFile::unzip_modeli_file(std::string path_to_modeli)
    {
        // Extract the modeliFile to *U*NZIP_DIR**/**filename without extension**
        fs::path modeliPath(path_to_modeli);
        fs::path extractPath = UNZIP_DIR / modeliPath.stem();
        Unzipper::Unzip(modeliPath.generic_string(), extractPath.generic_string());
        m_unzip_dir = extractPath.generic_string();
    }
    void ModeliFile::parse_modeli_xml()
    {
        // Load the Channel links
        boost::property_tree::ptree channel_link_xml;
        pt::read_xml((fs::path(m_unzip_dir) / "ChannelLinks.xml").generic_string(), channel_link_xml);
        // Parse the channel links
        for (auto c : channel_link_xml.get_child("ChannelLinks"))
        {
            // Only add the channel link if it is actually linked
            if (c.second.get<bool>("Linked"))
            {
                Simulation::ChannelLink channelLink;
                channelLink.Factor = c.second.get<double>("ScaleFactor");
                channelLink.Shift = c.second.get<double>("Offset");
                channelLink.MasterInstanceName = c.second.get<std::string>("MasterChannel.DataSourceName");
                channelLink.MasterValueRef = c.second.get<unsigned int>("MasterChannel.ValueRef");
                channelLink.SlaveInstanceName = c.second.get<std::string>("SlaveChannel.DataSourceName");
                channelLink.SlaveValueRef = c.second.get<unsigned int>("SlaveChannel.ValueRef");
                m_channel_links.push_back(std::move(channelLink));
            }
        }
        // Load the fmus
        boost::property_tree::ptree description_xml;
        pt::read_xml((fs::path(m_unzip_dir) / "Description.xml").generic_string(), description_xml);
        for (auto source : description_xml.get_child("Description.DataSources"))
        {
            // Only fmu datasources
            if (source.second.get<std::string>("SourceType") == "FMUDataSource")
            {
                m_model_for_instance.emplace(source.second.get<std::string>("InstanceName"),
                                             m_file_manager->add_model(m_unzip_dir + "/" + source.second.get<std::string>("Location")));
            }
        }
    }
}