#pragma once
#include <memory>
#include <string>
#include <vector>

namespace miniz_cpp
{
    class zip_file;
}

/// Can extract a zip file which contains a folder structure
class Unzipper
{
public:
    /// Extracts the zip file to the extractDir
    static void Unzip(std::string path, std::string unzipDir);
    /// Extracts the zip file to the extractDir
    static void Unzip(std::vector<unsigned char> buffer, std::string unzipDir);
private:
    /// Unzip from a zip_file to unzipDir.
    static void Unzip(std::unique_ptr<miniz_cpp::zip_file> zipFile, std::string unzipDir);
};