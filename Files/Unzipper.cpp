#include "Unzipper.hpp"
#include "boost/filesystem.hpp"
#include "zip_file.hpp"

void Unzipper::Unzip(std::string zipFile, std::string unzipDir)
{
	Unzip(std::make_unique<miniz_cpp::zip_file>(zipFile), unzipDir);
}

void Unzipper::Unzip(std::vector<unsigned char> buffer, std::string unzipDir)
{
	Unzip(std::make_unique<miniz_cpp::zip_file>(std::move(buffer)), unzipDir);
}

void Unzipper::Unzip(std::unique_ptr<miniz_cpp::zip_file> zipFile, std::string unzipDir)
{
	namespace fs = boost::filesystem;
	// Use boost path :)
	fs::path unzipPath(unzipDir);
	// Create folder structure, ifstream does not create them for us
	for (auto info : zipFile->infolist())
	{
		fs::path folder = (unzipPath / info.filename).parent_path();
		bool res = fs::create_directories(folder);
	}
	// Unzip
	zipFile->extractall(unzipDir);
}
