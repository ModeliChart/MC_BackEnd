#include "ModelDescription.h"
#include "boost/filesystem.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include <iostream>

namespace fs = boost::filesystem;
namespace pt = boost::property_tree;

namespace Files
{
    ModelDescription::ModelDescription()
    {
    }
    ModelDescription::~ModelDescription()
    {
    }

    void ModelDescription::load()
    {
        // Create & load ptree
        std::string xmlPath = m_model_dir + "/modelDescription.xml";
        if (!fs::exists(xmlPath))
        {
            // Do not crash. Load nothing.
            return;
        }
        pt::ptree desc;
        pt::read_xml(xmlPath, desc);
        // Load model information
        m_model_name = desc.get<std::string>("fmiModelDescription.CoSimulation.<xmlattr>.modelIdentifier");
        m_guid = desc.get<std::string>("fmiModelDescription.<xmlattr>.guid");
        // The valuereferences
        for (auto scalar_var : desc.get_child("fmiModelDescription.ModelVariables"))
        {
            // Simplify the checking and adding a value reference
            auto check_and_add = [scalar_var](std::string varType, std::vector<unsigned int>& vr_vector)
            {
                auto real_var = scalar_var.second.get_child_optional(varType);
                if (!real_var)
                {
                    return false;
                }
                else
                {
                    vr_vector.push_back(scalar_var.second.get<unsigned int>("<xmlattr>.valueReference"));
                    return true;
                }
            };
            // C++ pattern matching of the variable type
            if (!check_and_add("Real", m_real_vrs)) { ; }
            else if (!check_and_add("Integer", m_int_vrs)) { ; }
            else if (!check_and_add("Boolean", m_bool_vrs)) { ; }
            else if (check_and_add("String", m_string_vrs)) { ; }
            else if (check_and_add("Enumeration", m_int_vrs)) { ; }
        }
    }

    void ModelDescription::set_model_dir(std::string model_dir)
    {
        m_model_dir = model_dir;
    }

    std::string ModelDescription::get_model_dir()
    {
        return m_model_dir;
    }

    std::string ModelDescription::get_binary_path()
    {
        // The pathToFmu of the binary depends on the system
        fs::path binaryPath = m_model_dir;
        binaryPath /=  "binaries";
#if __linux__
#if __arm__
        binaryPath /= "arm-linux-gnueabihf";
#elif  __x86_64__ || __ppc64__
        binaryPath /= "linux64";
#else
        binaryPath /= "linux32";
#endif
#elif _WIN64
        binaryPath /= "win64";
#elif _WIN32
        binaryPath /= "win32";
#else
#error system ist not supported
#endif
        binaryPath /= m_model_name;
        // Return canonical (absolute) pathToFmu.
        // Don not use relative paths for dynamic library loading (security)!
        return fs::weakly_canonical(binaryPath).generic_string();
    }

    std::string ModelDescription::get_model_name()
    {
        return m_model_name;
    }

    std::string ModelDescription::get_guid()
    {
        return m_guid;
    }
    std::string ModelDescription::get_resource_uri()
    {
        // Create prefix
        std::string prefix = "file:///";	// mandatory according to the fmi doc
        // Create pathToFmu object
        std::string resourceDir = m_model_dir + "/resources";
        // Append absolute pathToFmu of the mod		
        return prefix + resourceDir;
    }
    std::vector<unsigned int> ModelDescription::get_int_vrs()
    {
        return m_int_vrs;
    }
    std::vector<unsigned int> ModelDescription::get_real_vrs()
    {
        return m_real_vrs;
    }
    std::vector<unsigned int> ModelDescription::get_bool_vrs()
    {
        return m_bool_vrs;
    }
    std::vector<unsigned int> ModelDescription::get_string_vrs()
    {
        return m_string_vrs;
    }
}