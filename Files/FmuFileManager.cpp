#include "FmuFileManager.h"
#include "Unzipper.hpp"
#include "boost/filesystem.hpp"

namespace fs = boost::filesystem;

namespace Files
{
    FmuFileManager::FmuFileManager()
    {
    }

    FmuFileManager::~FmuFileManager()
    {
    }

    ModelDescription FmuFileManager::add_model(std::vector<unsigned char> buffer)
    {
        // Unzip the fmu from byte buffer
        auto unzip_dir = create_unzip_dir();
        Unzipper::Unzip(std::move(buffer), unzip_dir);
        return add_from_unzipped(unzip_dir);
    }

    ModelDescription FmuFileManager::add_model(std::string path)
    {
        // Unzip the fmu from byte buffer
        auto unzip_dir = create_unzip_dir();
        Unzipper::Unzip(path, unzip_dir);
        return add_from_unzipped(unzip_dir);
    }

    void FmuFileManager::remove_model(std::string model_name)
    {
        if (m_models.count(model_name) == 1)
        {
            fs::remove_all(m_models[model_name].get_model_dir());
            m_models.erase(model_name);
        }
    }

    std::vector<ModelDescription> FmuFileManager::get_all()
    {
        std::vector<ModelDescription> retval;
        for (auto element : m_models)
        {
            retval.push_back(element.second);
        }
        return retval;
    }

    void FmuFileManager::clear()
    {
        fs::remove_all(MODELS_DIR);
        m_models.clear();
    }

    std::string FmuFileManager::create_unzip_dir()
    {
        fs::path unzip_path;
        do
        {
            unzip_path /= UNZIP_DIR;
            unzip_path /= fs::unique_path();
        } while (fs::exists(unzip_path));
        fs::create_directories(unzip_path);
        return unzip_path.generic_string();
    }

    ModelDescription FmuFileManager::add_from_unzipped(std::string unzip_dir)
    {
        // Load the model description
        ModelDescription model;
        model.set_model_dir(unzip_dir);
        model.load();
        // Check if the model exists
        if (m_models.count(model.get_model_name()) == 1)
        {
            // Clean temp
            fs::remove_all(unzip_dir);
            // Return existing instance
            return m_models[model.get_model_name()];
        }
        else
        {
            // Move to the MODELS dir
            fs::path new_dir;
            new_dir /= MODELS_DIR;
            new_dir /= model.get_model_name();
            // Windows requires the parent path to exist but the leaf directory must not exist!
            fs::create_directories(new_dir);
            fs::remove_all(new_dir);
            fs::rename(fs::path(unzip_dir), new_dir);
            model.set_model_dir(new_dir.generic_string());
            // Return the new model
            m_models.emplace(model.get_model_name(), model);
            return model;
        }
    }
}