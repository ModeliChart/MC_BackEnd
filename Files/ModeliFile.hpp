#pragma once
#include "ChannelLink.h"
#include "FmuFileManager.h"
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace Files
{
    /// Extracts a modeli file and the included information.
    /// The extracted files will be removed when the instance is destructed.
    class ModeliFile
    {
    public:
        /// Create a new instance from the given file location.
        ModeliFile(std::string path_to_modeli);
        ModeliFile(const ModeliFile&) = delete;
        void operator=(const ModeliFile&) = delete;
        ~ModeliFile();

        /// Extracts the channel links from the xml file in the modeli file
        std::vector<Simulation::ChannelLink> get_channel_links();
        /// Extracts the path and instancName of th fmus
        std::shared_ptr<FmuFileManager> get_fmu_file_manager();
        std::map<std::string, ModelDescription> get_model_for_instance();
    private:
        // Where the modeli file will be unzipped
        const std::string UNZIP_DIR = "UNZIP";
        std::string m_unzip_dir;
        // models & instance names
        std::shared_ptr<FmuFileManager> m_file_manager;
        std::map<std::string, ModelDescription> m_model_for_instance;
        std::vector<Simulation::ChannelLink> m_channel_links;
        // Unzip the contents of the modeli file
        void unzip_modeli_file(std::string path_to_modeli);
        // Parse the xml file and store the relevant data
        void parse_modeli_xml();
    };
}