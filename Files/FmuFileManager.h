#pragma once
#include "ModelDescription.h"
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace Files
{
    /// Manages the FmuFiles which are contained in the MODELS directory.
    class FmuFileManager
    {
    public:
        FmuFileManager();
        FmuFileManager(const FmuFileManager&) = delete;
        ModelDescription& operator=(const FmuFileManager&) = delete;
        ~FmuFileManager();

        /// Add a new ModelDescription from a buffer and return it
        ModelDescription add_model(std::vector<unsigned char> buffer);
        /// Add a new ModelDescription from the filesystem
        ModelDescription add_model(std::string path);
        /// Removes the ModelDescription from the filesystem
        void remove_model(std::string model_name);
        /// Returns a vector of all model descriptions
        std::vector<ModelDescription> get_all();
        /// Clears the models
        void clear();
    private:
        const std::string MODELS_DIR = "MODELS";
        const std::string UNZIP_DIR = "UNZIP";
        // Store model_name, decription
        std::map<std::string, ModelDescription> m_models;
        // Creates an empty directory for unzipping the fmu.
        // The path to the directory is returned.
        std::string create_unzip_dir();

        // Add a model description that has been unzipped to the unzip_dir
        ModelDescription add_from_unzipped(std::string unzip_dir);     
    };

}