#include "stdafx.h"
#include "CppUnitTest.h"
#include "QueueDispatcher.h"
#include <iostream>
#include <future>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std::placeholders;

namespace BackendTests
{
    TEST_CLASS(QueueDispatcherTest)
    {
    private:

    public:
        TEST_METHOD(TestQueueDispatcher)
        {
            // Test functions to dispatch
            auto addOne = [](int n)
            {
                return n + 1;
            };
            auto get42 = [addOne]()
            {
                return addOne(41);
            };
            Utility::QueueDispatcher dispatcher;
            // Empty & automatic
            Assert::IsFalse(dispatcher.execute_one());
            // automatic execution in auto mode and two threads
            const int COUNT = 1000;
            std::thread one([&]()
            {
                for (int i = 0; i < COUNT; i++)
                {
                    auto addOneToI = [addOne, i]()
                    {
                        return addOne(i);
                    };
                    auto res = dispatcher.dispatch_fn<int>(addOneToI);
                    Assert::AreEqual(i + 1, res.get());
                }
            });
            // False in automatic mode with data available
            Assert::IsFalse(dispatcher.execute_one());
            std::thread two([&]()
            {
                for (int i = COUNT; i > 0; i--)
                {
                    auto addOneToI = [addOne, i]()
                    {
                        return addOne(i);
                    };
                    auto res = dispatcher.dispatch_fn<int>(addOneToI);
                    Assert::AreEqual(i + 1, res.get());
                }
            });
            one.join();
            two.join();
            // Test manual mode
            dispatcher.set_manual();
            // False on empty
            Assert::IsFalse(dispatcher.execute_one());
            // Add an action manually
            auto dispFuture = dispatcher.dispatch_fn<int>(get42);
            Assert::IsTrue(dispatcher.execute_one());
            Assert::AreEqual(42, dispFuture.get());
            // Test finish
            dispatcher.finish();
            dispatcher.dispatch_void([]() { ; });
            Assert::IsFalse(dispatcher.execute_one());
        }

    };
}