#include "stdafx.h"
#include "CppUnitTest.h"
#include "ConcurrentQueue.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BackendTests
{
    TEST_CLASS(ConcurrentQueueTest)
    {
    public:

        TEST_METHOD(TestWaitableQueuePush_n_Pop)
        {
            Utility::ConcurrentQueue<int> queue;
            const int COUNT = 1000;
            // Produce one more than consuming
            std::thread consumer([&]()
            {
                for (int i = 0; i < COUNT; i++)
                {
                    queue.push(i);
                }
            });
            std::thread producer([&]()
            {
                for (int i = 0; i < COUNT + 1; i++)
                {
                    Assert::AreEqual(i, queue.wait_and_pop().get());
                }
            });
            // Wait for the threads
            producer.join();
            consumer.join();
            // One more availabe to test pop
            Assert::AreEqual(COUNT, queue.pop().get());
            // Test waitfor
            Assert::IsTrue(!queue.waitfor_and_pop(10));
            std::thread popNoTimeout([&]()
            {
                Assert::AreEqual(42, queue.waitfor_and_pop(100).get());
            });
            // Gets poped by popNoTimeout thread
            queue.push(42);
            popNoTimeout.join();
            // Empty queue -> test finish
            queue.finish();
            queue.push(42);
            Assert::IsTrue(!queue.pop());
        }

    };
}