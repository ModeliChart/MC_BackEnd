#include "ModeliGrpcServer.h"
#include "ChannelLink.h"
#include "FmuFileManager.h"
#include "Simulator.hpp"

namespace ph = std::placeholders;

ModeliGrpcServer::ModeliGrpcServer(std::unique_ptr<Simulation::Simulator> simulator, std::shared_ptr<Files::FmuFileManager> file_manager) :
    m_simulator(std::move(simulator)),
    m_fmu_file_manager(std::move(file_manager))
{
    // Register the callbacks
    m_simulator->RegisterLogCallback(std::bind(&ModeliGrpcServer::LogArrived, this, ph::_1, ph::_2, ph::_3, ph::_4));
    m_simulator->RegisterNewValuesCallback(std::bind(&ModeliGrpcServer::ValuesArrived, this,
                                                     ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6, ph::_7, ph::_8, ph::_9, ph::_10));
}

ModeliGrpcServer::~ModeliGrpcServer()
{
    // Remove the simulation, the callbacks will be unavailable after destruction
    // Prevents crashing
    m_simulator.reset();
}

void ModeliGrpcServer::Shutdown()
{
    m_logs.finish();
    m_new_values.finish();
}

Status ModeliGrpcServer::Play(ServerContext * context, const::ModeliRpc::PlayRequest * request, ::ModeliRpc::PlayResponse * response)
{
    response->set_status(m_simulator->Play());
    return Status::OK;
}

Status ModeliGrpcServer::PlayFast(ServerContext * context, const::ModeliRpc::PlayFastRequest * request, ::ModeliRpc::PlayFastResponse * response)
{
    response->set_status(m_simulator->PlayFast(request->time()));
    return Status::OK;
}

Status ModeliGrpcServer::Pause(ServerContext * context, const::ModeliRpc::PauseRequest * request, ::ModeliRpc::PauseResponse * response)
{
    m_simulator->Pause();
    return Status::OK;
}

Status ModeliGrpcServer::Stop(ServerContext * context, const::ModeliRpc::StopRequest * request, ::ModeliRpc::StopResponse * response)
{
    response->set_status(m_simulator->Stop());
    return Status::OK;
}

Status ModeliGrpcServer::AddFmu(ServerContext * context, ServerReader<::ModeliRpc::AddFmuRequest>* reader, ::ModeliRpc::AddFmuResponse * response)
{
    std::string instanceName = "";
    // Protobuf uses std::string to store bytes, so for convenience use a string as buffer.
    std::vector<unsigned char> buffer;
    ModeliRpc::AddFmuRequest request;
    // Read the chunks
    while (reader->Read(&request))
    {
        if (instanceName == "")
        {
            instanceName = request.instance_name();
        }
        // Intial assignments
        if (buffer.size() == 0)
        {
            buffer.reserve(request.total_size());
        }
        // Insert recceived data
        buffer.insert(buffer.end(), request.chunk().begin(), request.chunk().end());
    }
    // Add to the file manager
    auto model_description = m_fmu_file_manager->add_model(std::move(buffer));
    // Try to add the fmu
    response->set_success(m_simulator->AddFmu(std::move(model_description), instanceName));
    return Status::OK;
}

Status ModeliGrpcServer::RemoveFmu(ServerContext * context, const::ModeliRpc::RemoveFmuRequest * request, ::ModeliRpc::RemoveFmuResponse * response)
{
    response->set_success(m_simulator->RemoveFMU(request->instance_name()));
    return Status::OK;
}

Status ModeliGrpcServer::AddChannelLink(ServerContext * context, const::ModeliRpc::AddChannelLinkRequest * request, ::ModeliRpc::AddChannelLinkResponse * response)
{
    // Create the channelLink
    using Simulation::ChannelLink;
    ChannelLink cl;
    auto rcl = request->channel_link();
    cl.MasterInstanceName = rcl.master_instance_name();
    cl.SlaveInstanceName = rcl.slave_instance_name();
    cl.MasterValueRef = rcl.master_vr();
    cl.SlaveValueRef = rcl.slave_vr();
    cl.Factor = rcl.factor();
    cl.Shift = rcl.offset();
    // Add it
    response->set_success(m_simulator->AddChannelLink(cl));
    return Status::OK;
}

Status ModeliGrpcServer::RemoveChannelLink(ServerContext * context, const::ModeliRpc::RemoveChannelLinkRequest * request, ::ModeliRpc::RemoveChannelLinkResponse * response)
{
    // Create the channelLink
    using Simulation::ChannelLink;
    ChannelLink cl;
    auto rcl = request->channel_link();
    cl.MasterInstanceName = rcl.master_instance_name();
    cl.SlaveInstanceName = rcl.slave_instance_name();
    cl.MasterValueRef = rcl.master_vr();
    cl.SlaveValueRef = rcl.slave_vr();
    cl.Factor = rcl.factor();
    cl.Shift = rcl.offset();
    // Remove it
    response->set_success(m_simulator->RemoveChannelLink(cl));
    return Status::OK;
}

Status ModeliGrpcServer::SetInt(ServerContext * context, const::ModeliRpc::SetIntRequest * request, ::ModeliRpc::SetIntResponse * response)
{
    std::vector<unsigned int> vrs(request->values().vrs().begin(), request->values().vrs().end());
    std::vector<int> values(request->values().values().begin(), request->values().values().end());
    response->set_status(m_simulator->SetInteger(request->instance_name(), vrs, values));
    return Status::OK;
}

Status ModeliGrpcServer::SetReal(ServerContext * context, const::ModeliRpc::SetRealRequest * request, ::ModeliRpc::SetRealResponse * response)
{
    std::vector<unsigned int> vrs(request->values().vrs().begin(), request->values().vrs().end());
    std::vector<double> values(request->values().values().begin(), request->values().values().end());
    response->set_status(m_simulator->SetReal(request->instance_name(), vrs, values));
    return Status::OK;
}

Status ModeliGrpcServer::SetBoolean(ServerContext * context, const::ModeliRpc::SetBoolRequest * request, ::ModeliRpc::SetBoolResponse * response)
{

    std::vector<unsigned int> vrs(request->values().vrs().begin(), request->values().vrs().end());
    std::vector<int> values(request->values().values().begin(), request->values().values().end());
    response->set_status(m_simulator->SetBoolean(request->instance_name(), vrs, values));
    return Status::OK;
}

Status ModeliGrpcServer::SetString(ServerContext * context, const::ModeliRpc::SetStringRequest * request, ::ModeliRpc::SetStringResponse * response)
{
    std::vector<unsigned int> vrs(request->values().vrs().begin(), request->values().vrs().end());
    std::vector<std::string> values(request->values().values().begin(), request->values().values().end());
    response->set_status(m_simulator->SetString(request->instance_name(), vrs, values));
    return Status::OK;
}

Status ModeliGrpcServer::NewValues(ServerContext * context, const::ModeliRpc::NewValuesRequest * request, ServerWriter<::ModeliRpc::NewValuesResponse>* writer)
{
    bool streamOpen = true;
    // Write to the stream until it is closed by the frontend
    while (streamOpen)
    {
        if (auto item = m_new_values.wait_and_pop())
        {
            streamOpen = writer->Write(*item);
        }
        else if (m_new_values.is_finished())
        {
            // No value available and no more will come
            break;
        }
    }
    return Status::OK;
}

Status ModeliGrpcServer::Log(ServerContext * context, const::ModeliRpc::LogRequest * request, ServerWriter<::ModeliRpc::LogResponse>* writer)
{
    bool streamOpen = true;
    // Write to the stream until it is closed by the frontend
    while (streamOpen)
    {
        if (auto item = m_logs.wait_and_pop())
        {
            streamOpen = writer->Write(*item);
        }
        else if (m_logs.is_finished())
        {
            // No value available and no more will come
            break;
        }
    }
    return Status::OK;
}

void ModeliGrpcServer::ValuesArrived(const std::string instanceName, double timestamp, std::vector<unsigned int> intVrs, std::vector<int> intValues, std::vector<unsigned int> realVrs, std::vector<double> realValues, std::vector<unsigned int> boolVrs, std::vector<int> boolValues, std::vector<unsigned int> stringVrs, std::vector<std::string> stringValues)
{
    ModeliRpc::NewValuesResponse response;
    response.set_instance_name(instanceName);
    response.set_timestamp(timestamp);
    // Create repeated fields
    using google::protobuf::RepeatedField;
    using google::protobuf::uint32;
    // int
    RepeatedField<unsigned int> int_vrs(intVrs.begin(), intVrs.end());
    RepeatedField<int> int_values(intValues.begin(), intValues.end());
    response.mutable_int_values()->mutable_vrs()->Swap(&int_vrs);
    response.mutable_int_values()->mutable_values()->Swap(&int_values);
    // real
    RepeatedField<unsigned int> real_vrs(realVrs.begin(), realVrs.end());
    RepeatedField<double> real_values(realValues.begin(), realValues.end());
    response.mutable_real_values()->mutable_vrs()->Swap(&real_vrs);
    response.mutable_real_values()->mutable_values()->Swap(&real_values);
    // bool
    RepeatedField<unsigned int> bool_vrs(boolVrs.begin(), boolVrs.end());
    RepeatedField<int> bool_values(boolValues.begin(), boolValues.end());
    response.mutable_bool_values()->mutable_vrs()->Swap(&bool_vrs);
    response.mutable_bool_values()->mutable_values()->Swap(&bool_values);
    // string
    RepeatedField<unsigned int> string_vrs(stringVrs.begin(), stringVrs.end());
    google::protobuf::RepeatedPtrField<std::string> string_values(stringValues.begin(), stringValues.end());
    response.mutable_string_values()->mutable_vrs()->Swap(&string_vrs);
    response.mutable_string_values()->mutable_values()->Swap(&string_values);
    // Buffer the values so the simulation does not have to wait for the network
    m_new_values.push(response);
}

void ModeliGrpcServer::LogArrived(std::string instanceName, int status, std::string category, std::string message)
{
    ModeliRpc::LogResponse response;
    response.set_instance_name(instanceName);
    response.set_status(status);
    response.set_message("Category: " + category + " Message: " + message);
    // Buffer the logs so the simulation does not get blocked
    m_logs.push(response);
}
