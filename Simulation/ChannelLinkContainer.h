#pragma once
#include <memory>
#include <string>
#include <list>
#include "ChannelLink.h"

namespace Simulation
{


    class FmuContainer;

    class ChannelLinkContainer
    {
    public:
        ChannelLinkContainer(std::shared_ptr<FmuContainer> fmus);
        ChannelLinkContainer(const ChannelLinkContainer&) = delete;
        void operator=(const ChannelLinkContainer&) = delete;
        ~ChannelLinkContainer();

        /// Adds the given channel link to the container
        bool AddChannelLink(ChannelLink link);

        /// Remove one specific channel link
        bool RemoveChannelLink(ChannelLink  link);

        /// Removes all channel links that use the fmu instance
        void RemoveAllForFmuInstance(std::string instanceName);

        void ExecuteChannelLinks();

    private:
        std::list<ChannelLink> m_links;
        std::shared_ptr<FmuContainer> m_fmus;

        bool contains_link(ChannelLink link);
    };
}