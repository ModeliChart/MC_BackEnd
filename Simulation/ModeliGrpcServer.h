#pragma once
#include "ModeliRpc.grpc.pb.h"
#include "ConcurrentQueue.h"

using grpc::Status;
using grpc::ServerContext;
using grpc::ServerWriter;
using grpc::ServerReader;

namespace Simulation
{
    class Simulator;
}

namespace Files
{
    class FmuFileManager;
}

/// This class implements the server side of the grpc definition.
/// It is in charge of controlling the simulation and sending feedback to the frontend.
class ModeliGrpcServer final : public ModeliRpc::ModeliBackend::Service
{
public:
    ModeliGrpcServer(std::unique_ptr<Simulation::Simulator> simulator, std::shared_ptr<Files::FmuFileManager> file_manager);
    ModeliGrpcServer(const ModeliGrpcServer&) = delete;
    void operator=(const ModeliGrpcServer&) = delete;
    ~ModeliGrpcServer();

    // Shutdown the streams
    void Shutdown();
    // Play the simulation infinetly in realtime
    Status Play(ServerContext* context, const ModeliRpc::PlayRequest* request, ModeliRpc::PlayResponse* response);
    // Play the simulation as fast as possible (fast forwad)
    Status PlayFast(ServerContext* context, const ModeliRpc::PlayFastRequest* request, ModeliRpc::PlayFastResponse* response);
    // Pause the simulation without resetting it
    Status Pause(ServerContext* context, const ModeliRpc::PauseRequest* request, ModeliRpc::PauseResponse* response);
    // Halt and reset the simulation
    Status Stop(ServerContext* context, const ModeliRpc::StopRequest* request, ModeliRpc::StopResponse* response);
    // Add a new Fmu to the simulation, transfer it via chunks
    Status AddFmu(ServerContext* context, ServerReader<ModeliRpc::AddFmuRequest>* reader, ModeliRpc::AddFmuResponse* response);
    // Remove a Fmu from the simulation
    Status RemoveFmu(ServerContext* context, const ModeliRpc::RemoveFmuRequest* request, ModeliRpc::RemoveFmuResponse* response);
    // Add a ChannelLink to the simulation
    Status AddChannelLink(ServerContext* context, const ModeliRpc::AddChannelLinkRequest* request, ModeliRpc::AddChannelLinkResponse* response);
    // Remove a ChannelLink from the simulation
    Status RemoveChannelLink(ServerContext* context, const ModeliRpc::RemoveChannelLinkRequest* request, ModeliRpc::RemoveChannelLinkResponse* response);
    // Set int values in one of the fmus
    Status SetInt(ServerContext* context, const ModeliRpc::SetIntRequest* request, ModeliRpc::SetIntResponse* response);
    // Set real values in one of the fmus
    Status SetReal(ServerContext* context, const ModeliRpc::SetRealRequest* request, ModeliRpc::SetRealResponse* response);
    // Set bool values in one of the fmus
    Status SetBoolean(ServerContext* context, const ModeliRpc::SetBoolRequest* request, ModeliRpc::SetBoolResponse* response);
    // Set string values in one of the fmus   
    Status SetString(ServerContext* context, const ModeliRpc::SetStringRequest* request, ModeliRpc::SetStringResponse* response);
    // Stream simulation results to the client
    Status NewValues(ServerContext* context, const ModeliRpc::NewValuesRequest* request, ServerWriter<ModeliRpc::NewValuesResponse>* writer);
    // Stream log messages to the client
    Status Log(ServerContext* context, const ModeliRpc::LogRequest* request, ServerWriter<ModeliRpc::LogResponse>* writer);

    /// Observer callback
    virtual void ValuesArrived(
        std::string instanceName,	///< The instanceName of the FMU
        double timestamp,	///< Timestamp (current time)
        std::vector<unsigned int> intVrs,
        std::vector<int> intValues,
        std::vector<unsigned int> realVrs,
        std::vector<double> realValues,
        std::vector<unsigned int> boolVrs,
        std::vector<int> boolValues,
        std::vector<unsigned int> stringVrs,
        std::vector<std::string> stringValues);

    /// Observer callback
    virtual void LogArrived(
        std::string instanceName,	///< The instanceName of the FMU
        int status,	///< Fmi2Status of the message
        std::string category,	///< Category of the message
        std::string message	///< The message that has been logged
    );
private:
    std::shared_ptr<Files::FmuFileManager> m_fmu_file_manager;
    // Execue the simulation commands
    std::unique_ptr<Simulation::Simulator> m_simulator;
    // Wait for logs to arrive
    Utility::ConcurrentQueue<ModeliRpc::LogResponse> m_logs;
    // Wait for values to arrive
    Utility::ConcurrentQueue<ModeliRpc::NewValuesResponse> m_new_values;
};