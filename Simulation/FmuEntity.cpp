#include "FmuEntity.h"

namespace Simulation
{

    FmuEntity::FmuEntity(std::string instance_name, Files::ModelDescription file, NativeFmu::LoggerCallback logger) :
        m_instance_name(instance_name),
        m_file(std::move(file)),
        m_logger(logger)
    {
        m_fmu = std::make_shared<NativeFmu::CoSimFmu>();
        m_fmu->SetBooleanVr(m_file.get_bool_vrs());
        m_fmu->SetIntegerVr(m_file.get_int_vrs());
        m_fmu->SetRealVr(m_file.get_real_vrs());
        m_fmu->SetStringVr(m_file.get_string_vrs());
    }

    std::string FmuEntity::GetInstanceName()
    {
        return m_instance_name;
    }

    std::weak_ptr<NativeFmu::CoSimFmu> FmuEntity::GetCoSimFmu()
    {
        return m_fmu;
    }

    bool FmuEntity::InstantiateFmu()
    {
        std::pair<bool, std::string> loadDllRes = m_fmu->LoadDll(m_file.get_binary_path());
        if (!loadDllRes.first)
        {
            m_logger(m_instance_name, fmi2Warning, "", "Failed LoadLibrary, Error: " + loadDllRes.second);
            return false;
        }

        // Setup callbacks and parameters
        namespace ph = std::placeholders;
        m_fmu->SetLoggerCallback(m_logger);
        // Instantiate
        if (!m_fmu->Instantiate(m_instance_name, m_file.get_guid(), m_file.get_resource_uri()))
        {
            m_logger(m_instance_name, fmi2Warning, "", "Instantiation failed.");
            return false;
        }
        else
        {
            return true;
        }
    }

    int FmuEntity::InitializeFmu(double start_time)
    {
        // No error estimation and no pre-defined stoptime -> 10.0 is meaningless
        int status = m_fmu->SetupExperiment(fmi2False, 0, start_time, fmi2False, 0);
        if (status == fmi2Error || status == fmi2Fatal)
        {
            return status;
        }
        // Switch to initialization mode until the first play action
        return m_fmu->EnterInitializationMode();
    }

    FmuEntity::~FmuEntity()
    {
    }
}