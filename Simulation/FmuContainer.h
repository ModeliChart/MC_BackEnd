#pragma once
#include "FmuEntity.h"
#include "boost/optional.hpp"
#include <map>
#include <memory>
#include <vector>

namespace Simulation
{
    /// Manages the the fmu entities.
    /// Makes sure each instance_name is only used once.
    class FmuContainer
    {
    private:
        // Identify the fmus via their instance names
        // Use heap memory to prevent destructor calls (releases external ressources)
        std::map<std::string, std::shared_ptr<FmuEntity>> m_entities;

    public:
        // Only possible with callbacks
        FmuContainer();
        // No copy or assign!
        FmuContainer(const FmuContainer&) = delete;
        FmuContainer(FmuContainer&&) = default;
        FmuContainer& operator=(const FmuContainer&) = delete;
        FmuContainer& operator=(FmuContainer&&) = default;
        ~FmuContainer();

        /// Add a fmu from the file and instantiate.
        /// Will return false if the file exists or instantiate failed.
        bool Add(std::shared_ptr<FmuEntity> entity);

        /// If it exists the instance will be removed.
        bool Remove(std::string instanceName);

        bool Contains(std::string instanceName);

        /// Returns a vector of all fmus that are managed by this container
        std::vector<std::shared_ptr<FmuEntity>> GetEntities();

        /// Returns a weak pointer to the entity. If the entity does not exist the ptr will be empty
        std::shared_ptr<FmuEntity> GetEntity(std::string instanceName);


    };  // class
}   // namespace