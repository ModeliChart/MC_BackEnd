#pragma once
#include <string>

namespace Simulation
{
	typedef struct
	{
		unsigned int SlaveValueRef;	// Ref of Settable Channel
		std::string SlaveInstanceName;	// InstanceName of the model including the Slave
		unsigned int MasterValueRef;	// Ref of Gettable Channel
		std::string MasterInstanceName;	// InstanceName of the model including the Master
		double Factor;					// m
		double Shift;					// n
	}ChannelLink;

	bool operator==(const Simulation::ChannelLink& lhs, const Simulation::ChannelLink& rhs);
}