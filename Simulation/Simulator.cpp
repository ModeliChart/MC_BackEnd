#include "Simulator.hpp"
#include "ChannelLink.h"
#include "FmuContainer.h"
#include "ChannelLinkContainer.h"
#include "FmuEntity.h"
// std
#include <chrono>
#include <iostream>
// boost
#include "boost/filesystem.hpp"
#include "boost/timer/timer.hpp"
// namespace alias
namespace fs = boost::filesystem;
namespace ph = std::placeholders;

namespace Simulation
{
    Simulator::Simulator()
    {
        m_fmus = std::make_shared<FmuContainer>();
        m_links = std::make_unique<ChannelLinkContainer>(m_fmus);
    }

    Simulator::~Simulator()
    {
        // Stop a possibly running simulation thread
        m_dispatcher.dispatch_void([&]()
        {
            m_stop = true;
        });
        // Make sure alle work as been dispatched before getting destroyed
        m_dispatcher.finish();
        m_dispatcher.set_manual();
        while (m_dispatcher.execute_one()) { ; }
        // Join the simulation thred at the end
        if (m_sim_thread.joinable())
        {
            m_sim_thread.join();
        }
    }

    int Simulator::start_simulation()
    {
        if (m_sim_thread.joinable())
        {
            // Resume simulation thread
            m_dispatcher.dispatch_void([&]()
            {
                m_pause = false;
            }).wait();
            return fmi2OK;
        }
        else
        {
            // Create new simulation thread
            return m_dispatcher.dispatch_fn<int>([&]()
            {
                // Get fmus ready
                for (auto entity : m_fmus->GetEntities())
                {
                    if (auto fmu = entity->GetCoSimFmu().lock())
                    {
                        int status = fmu->ExitInitializationMode();
                        if (status == fmi2Error || status == fmi2Fatal)
                        {
                            return status;
                        }
                    }
                }
                // Run simulation
                m_sim_thread = std::thread([&]()
                {
                    // Get full control of dispatching additional work in simulation loop
                    m_dispatcher.set_manual();
                    // Simulate
                    simulation_loop();
                    // Since no loop is running, automatic dispatch is required.
                    m_dispatcher.set_automatic();
                });
                return static_cast<int>(fmi2OK);
            }).get();
        }
    }

    void Simulator::simulation_loop()
    {
        // If playFast is activated the timer will have an offset (it doesn't run as fast)
        double timer_offset = 0;
        // CPU timer is steady on windows, linux and mac
        boost::timer::cpu_timer timer;
        timer.start();
        // Loop until signal has been given, or the endTime(including) has been reached
        while (!m_stop)
        {
            // Simulate fmus
            m_links->ExecuteChannelLinks();
            for (auto entity : m_fmus->GetEntities())
            {
                if (auto fmu = entity->GetCoSimFmu().lock())
                {
                    int dostepStatus = fmu->DoStep(
                        m_sim_time,
                        STEP_SIZE,
                        fmi2False);
                    if (dostepStatus == fmi2Error || dostepStatus == fmi2Fatal)
                    {
                        return;	// Simulation no longer possible
                    }
                }
            }
            // Transfer the results, the server won't block (uses queue as buffer)  
            sendValues(m_sim_time
            );
            m_sim_time += STEP_SIZE;

            // Dispatch work depending on mode
            if (m_play_fast_for > 0)
            {
                // Stop timer so we don't have to wait forever
                timer.stop();
                m_play_fast_for -= STEP_SIZE;
                timer_offset += STEP_SIZE;
                // Dispatch one new workload
                m_dispatcher.execute_one();
            }
            else
            {
                // Resume timer for realtime execution after playfast
                timer.resume();
                int xyz = 0;
                // Keep up / wait for realtime
                std::chrono::duration<double> sim_time_with_offset{ m_sim_time - timer_offset };
                for (auto measured = std::chrono::nanoseconds{ timer.elapsed().wall }; 
                     measured < sim_time_with_offset;
                     measured = std::chrono::nanoseconds{ timer.elapsed().wall })
                {
                    if (sim_time_with_offset - measured > std::chrono::milliseconds{ SPIN_WAIT_EPS })
                    {
                        // Time to dispatch work
                        if (!m_dispatcher.execute_one())
                        {
                            // No work available let the CPU breath (Linux uses nanosleep :))
                            std::this_thread::sleep_for(std::chrono::milliseconds(SIM_SLEEP_MS));
                        }
                    }
                    else
                    {
                        xyz++;   // Do nothing but looping and measuring the current time
                    }
                }
            }
            if (m_pause)
            {
                timer.stop();
                while (m_pause)
                {
                    // Keep dispatching new workload (so resume is possible)
                    if (!m_dispatcher.execute_one())
                    {
                        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_IN_PAUSE_MS));
                    }
                }
                timer.resume();
            }
        }
    }

    void Simulator::sendValues(double current_time)
    {
        for (auto entity : m_fmus->GetEntities())
        {
            if (auto fmu = entity->GetCoSimFmu().lock())
            {
                // Send the values			
                onValuesArrived(entity->GetInstanceName(), current_time,
                                fmu->GetIntegerVr(), fmu->GetInteger().second,
                                fmu->GetRealVr(), fmu->GetReal().second,
                                fmu->GetBooleanVr(), fmu->GetBoolean().second,
                                fmu->GetStringVr(), fmu->GetString().second);
            }
        }
    }

    int Simulator::Play()
    {
        // Play infinite
        m_dispatcher.dispatch_void([&]()
        {
            m_play_fast_for = 0;
        }).wait();
        // Will wait for the dispatch
        return start_simulation();
    }

    int Simulator::PlayFast(double interval_sec)
    {
        m_dispatcher.dispatch_void([&]()
        {
            m_play_fast_for += interval_sec;
        }).wait();
        // Will wait for the dispatch
        return start_simulation();
    }

    // Stop the simulation thread
    void Simulator::Pause()
    {
        // Dispatch pause
        m_dispatcher.dispatch_void([&]()
        {
            m_pause = true;
        }).wait();
    }

    // Stop the thread and reset the simulation
    int Simulator::Stop()
    {
        // Stop simulation
        m_dispatcher.dispatch_void([&]()
        {
            m_stop = true;
        }).wait();
        if (m_sim_thread.joinable())
        {
            m_sim_thread.join();
        }
        return m_dispatcher.dispatch_fn<int>([&]()
        {
            // Reset simualtion state
            m_sim_time = 0;
            m_pause = false;
            m_stop = false;
            // Reset all the fmus
            for (auto entity : m_fmus->GetEntities())
            {
                if (auto fmu = entity->GetCoSimFmu().lock())
                {
                    // int status = fmu->Reset(); Fails for simulink fmus
                    //  Terminate and free               
                    int terminateRes = fmu->Terminate();
                    if (terminateRes == fmi2Error || terminateRes == fmi2Fatal)
                    {
                        return terminateRes;
                    }
                    fmu->FreeInstance();
                    // Reinstantiate
                    if (!entity->InstantiateFmu())
                    {
                        return static_cast<int>(fmi2Fatal);
                    }
                    // Always leave the fmu in initialized state before simulation
                    int initFmuResult = entity->InitializeFmu(m_sim_time);
                    if (initFmuResult == fmi2Error || initFmuResult == fmi2Fatal)
                    {
                        return initFmuResult;
                    }
                }
            }
            return static_cast<int>(fmi2OK);
        }).get();
    }

    void Simulator::onValuesArrived(const std::string instanceName, double timestamp, const std::vector<unsigned int> intVrs, const std::vector<int> intValues, const std::vector<unsigned int> realVrs, const std::vector<double> realValues, const std::vector<unsigned int> boolVrs, const std::vector<int> boolValues, const std::vector<unsigned int> stringVrs, const std::vector<const char*> stringValues)
    {
        // Convert strings
        std::vector<std::string> string_vals(stringValues.begin(), stringValues.end());
        // Invoke each observer
        m_values_callback(
            instanceName,
            timestamp,
            intVrs,
            intValues,
            realVrs,
            realValues,
            boolVrs,
            boolValues,
            stringVrs,
            string_vals
        );
    }

    void Simulator::onLog(std::string instanceName, int status, std::string category, std::string message)
    {
        // Invoke each observer
        m_log_callback(instanceName, status, category, message);
    }

    bool Simulator::AddChannelLink(ChannelLink channelLink)
    {
        return m_dispatcher.dispatch_fn<bool>([&]()
        {
            return m_links->AddChannelLink(channelLink);
        }).get();
    }

    bool Simulator::RemoveChannelLink(ChannelLink channelLink)
    {
        return m_dispatcher.dispatch_fn<bool>([&]()
        {
            return m_links->RemoveChannelLink(channelLink);

        }).get();
    }

    bool Simulator::FmuInstanceExists(std::string instance_name)
    {
        return m_dispatcher.dispatch_fn<bool>([&]()
        {
            return m_fmus->Contains(instance_name);
        }).get();
    }

    bool Simulator::AddFmu(Files::ModelDescription model, std::string instance_name)
    {
        // Fail early if instance name exists (asking for state -> dispatch)
        if (FmuInstanceExists(instance_name))
        {
            return false;
        }
        // Heavy instantiation work outside the simulation
        std::shared_ptr<FmuEntity> entity = std::make_shared<FmuEntity>(
            instance_name,
            std::move(model),
            std::bind(&Simulator::onLog, this, ph::_1, ph::_2, ph::_3, ph::_4));
        if (!entity->InstantiateFmu())
        {
            return false;
        }
        return m_dispatcher.dispatch_fn<bool>([&]()
        {
            // Initialize must be synchronized with simulation
            int initRes = entity->InitializeFmu(m_sim_time);
            if (initRes == fmi2Error || initRes == fmi2Fatal)
            {
                return false;
            }
            // When simulation is running fmu must exit initialization mode
            if (m_sim_thread.joinable())
            {
                if (auto fmu = entity->GetCoSimFmu().lock())
                {
                    int status = fmu->ExitInitializationMode();
                    if (status == fmi2Error || status == fmi2Fatal)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            // Fmu ready to add
            return m_fmus->Add(entity);
        }).get();
    }

    bool Simulator::RemoveFMU(std::string instanceName)
    {
        return m_dispatcher.dispatch_fn<bool>([&]()
        {
            return m_fmus->Remove(instanceName);
        }).get();
    }
    int Simulator::SetInteger(std::string instance_name, std::vector<unsigned int> vr, std::vector<int> values)
    {
        if (auto entity = m_fmus->GetEntity(instance_name))
        {
            if (auto fmu = entity->GetCoSimFmu().lock())
            {
                fmu->SetInteger(vr.data(), vr.size(), values.data());
            }
        }
        else
        {
            onLog(instance_name, fmi2Warning, "", "Cannot set integer values, instance not found.");
            return fmi2Warning;
        }
    }
    int Simulator::SetReal(std::string instance_name, std::vector<unsigned int> vr, std::vector<double> values)
    {
        if (auto entity = m_fmus->GetEntity(instance_name))
        {
            if (auto fmu = entity->GetCoSimFmu().lock())
            {
                fmu->SetReal(vr.data(), vr.size(), values.data());
            }
        }
        else
        {
            onLog(instance_name, fmi2Warning, "", "Cannot set real values, instance not found.");
            return fmi2Warning;
        }
    }
    int Simulator::SetBoolean(std::string instance_name, std::vector<unsigned int> vr, std::vector<int> values)
    {
        if (auto entity = m_fmus->GetEntity(instance_name))
        {
            if (auto fmu = entity->GetCoSimFmu().lock())
            {
                fmu->SetBoolean(vr.data(), vr.size(), values.data());
            }
        }
        else
        {
            onLog(instance_name, fmi2Warning, "", "Cannot set boolean values, instance not found.");
            return fmi2Warning;
        }
    }
    int Simulator::SetString(std::string instance_name, std::vector<unsigned int> vr, std::vector<std::string> values)
    {
        if (auto entity = m_fmus->GetEntity(instance_name))
        {
            if (auto fmu = entity->GetCoSimFmu().lock())
            {
                std::vector<const char*> converted;
                for (auto element : values)
                {
                    converted.push_back(element.c_str());
                }
                fmu->SetString(vr.data(), vr.size(), converted.data());
            }
        }
        else
        {
            onLog(instance_name, fmi2Warning, "", "Cannot set string value, instance not found.");
            return fmi2Warning;
        }
    }
    void Simulator::RegisterNewValuesCallback(NewValuesCallback callback)
    {
        m_values_callback = callback;
    }
    void Simulator::RegisterLogCallback(LogCallback callback)
    {
        m_log_callback = callback;
    }
}