#include "ChannelLinkContainer.h"
#include "CoSimFmu.h"
#include "FmuContainer.h"
#include <algorithm>

namespace Simulation
{

    ChannelLinkContainer::ChannelLinkContainer(std::shared_ptr<FmuContainer> fmus) : m_fmus(fmus)
    {
    }


    ChannelLinkContainer::~ChannelLinkContainer()
    {
    }

    bool ChannelLinkContainer::AddChannelLink(ChannelLink link)
    {
        // No equal channel links shall exist
        if (contains_link(link))
        {
            return false;
        }
        else
        {
            if (m_fmus->Contains(link.MasterInstanceName) && m_fmus->Contains(link.SlaveInstanceName))
            {
                m_links.push_back(link);
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    bool ChannelLinkContainer::RemoveChannelLink(ChannelLink link)
    {
        if (contains_link(link))
        {
            m_links.remove(link);
            return true;
        }
        else
        {
            return false;
        }
    }

    void ChannelLinkContainer::RemoveAllForFmuInstance(std::string instanceName)
    {
        m_links.remove_if([instanceName](ChannelLink link)
        {
            if (link.MasterInstanceName == instanceName) { return true; }
            else if (link.SlaveInstanceName == instanceName) { return true; }
            else { return false; }
        });
    }

    void Simulation::ChannelLinkContainer::ExecuteChannelLinks()
    {
        // Perform each link
        for (ChannelLink link : m_links)
        {
            // Find the fmus
            auto masterEntity = m_fmus->GetEntity(link.MasterInstanceName);
            auto slaveEntity = m_fmus->GetEntity(link.SlaveInstanceName);
            if (!masterEntity || !slaveEntity)
            {
                continue;
            }
            auto master = masterEntity->GetCoSimFmu().lock();
            auto slave = slaveEntity->GetCoSimFmu().lock();
            if (!master || !slave)
            {
                continue;
            }
            // Calculate and set values
            double masterValue;
            int res = master->GetReal(&link.MasterValueRef, 1, &masterValue);
            if (res != fmi2OK)
            {
                // Failed getting value -> try next one
                continue;
            }
            fmi2Real slaveValue = masterValue * link.Factor + link.Shift;
            slave->SetReal(&link.SlaveValueRef, 1, &slaveValue);
        }
    }
    bool ChannelLinkContainer::contains_link(ChannelLink link)
    {
        if (std::find(m_links.begin(), m_links.end(), link) == m_links.end())
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}