#include "ChannelLink.h"

namespace Simulation
{
	// Equals if the InstanceNames and valueReferences match
	bool operator==(const ChannelLink& lhs, const ChannelLink& rhs)
	{
		if (lhs.SlaveValueRef != rhs.SlaveValueRef) return false;
		if (lhs.MasterValueRef != rhs.MasterValueRef) return false;
		if (lhs.SlaveInstanceName != rhs.SlaveInstanceName) return false;
		if (lhs.MasterInstanceName != rhs.MasterInstanceName) return false;
		return true;
	}
}