#include "FmuContainer.h"

namespace Simulation
{

    FmuContainer::FmuContainer()
    {
    }


    FmuContainer::~FmuContainer()
    {
    }

    bool FmuContainer::Add(std::shared_ptr<FmuEntity> entity)
    {
        if (!Contains(entity->GetInstanceName()))
        {
            m_entities.emplace(entity->GetInstanceName(), entity);
            return true;
        }
        else
        {
            return false;
        }
    }

    bool FmuContainer::Remove(std::string instanceName)
    {
        // Free the fmu
        if (Contains(instanceName))
        {
            // Remove fmu first to free it, then the file
            m_entities.erase(instanceName);
            return true;
        }
        else
        {
            return false;
        }
    }

    bool FmuContainer::Contains(std::string instanceName)
    {
        return m_entities.count(instanceName) == 1;
    }

    std::vector<std::shared_ptr<FmuEntity>> FmuContainer::GetEntities()
    {
        std::vector<std::shared_ptr<FmuEntity>> fmus;
        for (auto iter : m_entities)
        {
            fmus.push_back(iter.second);
        }
        return fmus;
    }

    std::shared_ptr<FmuEntity> FmuContainer::GetEntity(std::string instanceName)
    {
        if (Contains(instanceName))
        {
            return m_entities[instanceName];
        }
        else
        {
            return nullptr;
        }
    }
}   // namespace