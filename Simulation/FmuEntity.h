#pragma once
#include "CoSimFmu.h"
#include "ModelDescription.h"
#include "BasicFmuCallbacks.h"
#include <memory>

namespace Simulation
{
    /// Keeps a CoSimFmu together with its model description.
    /// Instantiation and Initialization of the fmu are simplified because they are kept together.
    class FmuEntity
    {
    public:
        FmuEntity(std::string instance_name, Files::ModelDescription model, NativeFmu::LoggerCallback logger);

        std::string GetInstanceName();

        /// Returns a reference to the fmu.
        std::weak_ptr<NativeFmu::CoSimFmu> GetCoSimFmu();

        /// Instantiates the fmu with the infromation passed to the constructor
        bool InstantiateFmu();

        /// Calls setup experiment with start_time and then enters initialization mode
        int InitializeFmu(double start_time);

        ~FmuEntity();
    private:
        std::string m_instance_name;
        Files::ModelDescription m_file;
        std::shared_ptr<NativeFmu::CoSimFmu> m_fmu;
        NativeFmu::LoggerCallback m_logger;
    };
}