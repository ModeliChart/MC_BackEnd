#pragma once
#include "ChannelLink.h"
#include "QueueDispatcher.h"
#include "ModelDescription.h"
#include "boost/optional.hpp"
#include <functional>
#include <list>
#include <thread>
#include <vector>

namespace Simulation
{
    class FmuContainer;
    class ChannelLinkContainer;

    // Huge signature but we must deliver all values at once
    /// Use this signature for propagating new Values
    typedef std::function<void(
        std::string instanceName,   ///<  The fmu instance for the values
        double timestamp,   ///< The simulation time of the values
        std::vector<unsigned int> int_vrs,
        std::vector<int> int_values,
        std::vector<unsigned int> real_vrs,
        std::vector<double> real_values,
        std::vector<unsigned int> bool_vrs,
        std::vector<int> bool_values,
        std::vector<unsigned int> string_vrs,
        std::vector<std::string> string_values
        )> NewValuesCallback;
    /// Callback for logging to the frontend
    typedef std::function<void(
        std::string instanceName,	///< The instanceName of the FMU
        int status,	///< Fmi2Status of the message
        std::string category,	///< Category of the message
        std::string message	///< The message that has been logged)
        )> LogCallback;

    class Simulator
    {
    public:
        Simulator();
        Simulator(Simulator&&) = default;
        Simulator(const Simulator&) = delete;
        Simulator& operator=(Simulator&&) = default;
        Simulator& operator=(const Simulator&) = delete;      
        ~Simulator();

        /// Runs the simulation in realtime with endtime set to DBL_MAX
        int Play();
        /// Runs the simulation as fast as possible until endTime is reached.
        int PlayFast(double interval_sec);
        /// Halts the simulation at the current time. Can be continued via play.
        void Pause();
        /// Halts the simulation and resets it to t=0.
        int Stop();

        /// Add a ChannelLink
        bool AddChannelLink(ChannelLink channelLink);
        /// Remove a ChannelLink
        bool RemoveChannelLink(ChannelLink channelLink);

        /// Returns true if the instance is already part of the simulation
        bool FmuInstanceExists(std::string instance_name);
        /// Add a fmu which was loaded by FmuFile class
        bool AddFmu(Files::ModelDescription model, std::string instanceName);
        /// Remove the FMU with matching instanceName
        bool RemoveFMU(std::string instanceName);

        // Additional layer for setting fmus so we control when it is dispatched
        int SetInteger(std::string instance_name, std::vector<unsigned int> vr, std::vector<int> values);
        int SetReal(std::string instance_name, std::vector<unsigned int> vr, std::vector<double> values);
        int SetBoolean(std::string instance_name, std::vector<unsigned int> vr, std::vector<int> values);
        int SetString(std::string instance_name, std::vector<unsigned int> vr, std::vector<std::string> values);

        /// When new values are available
        void RegisterNewValuesCallback(NewValuesCallback callback);
        /// When a new log messaage is available
        void RegisterLogCallback(LogCallback callback);

    private:
        // Container
        std::shared_ptr<FmuContainer> m_fmus;
        std::unique_ptr<ChannelLinkContainer> m_links;

        // All work that modifies this classes state must be dispatched.
        // While simulation_thread is running use the manual_mode.
        Utility::QueueDispatcher m_dispatcher;

        // Execeutes the simulation: DoStep, pushes the values into storage, performes ChannelLinks
        std::thread m_sim_thread;
        // Simulation settings / state
        bool m_pause = false;
        bool m_stop = false;    // Reset to false after joining the thread
        double m_play_fast_for = 0;
        const long SLEEP_IN_PAUSE_MS = 100;  // Sleep length when pause is true
        const double STEP_SIZE = 0.01;
        const int SIM_SLEEP_MS = 1;     // Sleep in simulation mode (if it is too fast)
        const int SPIN_WAIT_EPS = 5;    // Spin no more dispatch or sleep when less than this time to realtime
        double m_sim_time = 0;

        // Not observable, just callbacks because the simulator will be owned by one server
        NewValuesCallback m_values_callback;
        LogCallback m_log_callback;

        /// Starts or resumes the simulation thread.
        /// Waits for the successfull execution from the queue.
        int start_simulation();
        /// Simulator loop runs in here
        void simulation_loop();
        /// Gets current values and calls onValuesArrived
        void sendValues(double current_time);

        // Spread the word: New Values arrived to all observers
        void onValuesArrived(const std::string instanceName, double timestamp,
                             const std::vector<unsigned int> intVrs, const std::vector<int> intValues,
                             const std::vector<unsigned int> realVrs, const std::vector<double> realValues,
                             const std::vector<unsigned int> boolVrs, const std::vector<int> boolValues,
                             const std::vector<unsigned int> stringVrs, const std::vector<const char*> stringValues);
        // Spread the word: A log has arrived (also used as callback for the fmus)
        void onLog(std::string instanceName, int status, std::string category, std::string message);
    };
}